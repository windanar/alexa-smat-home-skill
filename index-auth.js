exports.handler = async  function (event, context) {

    console.log('-------------------------------');
    console.log(JSON.stringify(event));
    console.log('-------------------------------');
    
    let response = {};
    
    if(event.httpMethod === 'GET') {
    
        const redirect_uri = event.queryStringParameters.redirect_uri + "?state=" + event.queryStringParameters.state + "&code=123sasdfasfasdasdSasda0FFs45678901234567890"
    
        console.log('--------------GET-----------------');
        response = {
            "statusCode": 302,
            "headers": {
                "Location": redirect_uri,
            },
            "body": ""
        }
    }
    else
    {
        console.log('---------------POST----------------');
        response = {
            "statusCode":200,
            "headers":null,
            "body":"{\"access_token\":\"hey...\",\"token_type\":\"bearer\"}"
        }
    }
    
    console.log(JSON.stringify(response));
    
    return response;
}