let mydeviceStatus = ["OFF", "OFF", "OFF", "OFF", "OFF"];

exports.handler = async function (request, context) {
    console.log('IN SKILL');
    console.log('------------REQUEST PAYLOAD START---------------------')
    console.log(JSON.stringify(request));
    console.log('------------REQUEST PAYLOAD END---------------------')

    //IoT Platform Access Token is in the request, which is retrived at the auth process
    //request.directive.payload.scope.token
    //Use this token to access devices and eecure actions

    //console.log('Token: ' + request.directive.payload.scope.token);
    console.log('Message Id: ' + request.directive.header.messageId);
    let response = {};

    switch (request.directive.header.namespace) {
        case 'Alexa.Discovery':
            response = DiscoverResponse(request.directive.header.messageId);
            break;
        case 'Alexa.PowerController':
            console.log('--DEVICE ID To Be Controlled: ' + request.directive.endpoint.endpointId);
            response = PowerControllerResponse(request.directive.header, request.directive.endpoint);
            break;
        case 'Alexa':
            console.log('==========================');
            console.log(mydeviceStatus[2]);
            console.log('==========================');
            response = ReportStateResponse(request.directive.header, request.directive.endpoint);
            break;

        default:
        // code
    }


    //Generate below response based on the devices has.
    //Here its hardcoded:

    console.log('----------RESPONSE PAYLOAD START-----------------------');
    console.log(JSON.stringify(response));
    console.log('----------RESPONSE PAYLOAD END-----------------------');
    console.log('END IN SKILL');
    return response;
};

function DiscoverResponse(messageId) {
    let devices = [];
    for (let x = 0; x < 5; x++) {
        const device = {
            "endpointId": "uniqueIDoftheendpoint_00_" + x,
            "manufacturerName": "WIN Manufacturer",
            "description": "Smart Light by WIN Manufacturer",
            "friendlyName": "Roof Top Light " + x,
            "displayCategories": [
                "LIGHT"
            ],
            "capabilities": [
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa.PowerController",
                    "version": "3",
                    "properties": {
                        "supported": [
                            {
                                "name": "powerState"
                            }
                        ],
                        "proactivelyReported": true,
                        "retrievable": true
                    }
                },
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa.EndpointHealth",
                    "version": "3",
                    "properties": {
                        "supported": [
                            {
                                "name": "connectivity"
                            }
                        ],
                        "proactivelyReported": true,
                        "retrievable": true
                    }
                },
                {
                    "type": "AlexaInterface",
                    "interface": "Alexa",
                    "version": "3"
                }
            ]
        };

        devices.push(device);
    }

    return {
        "event": {
            "header": {
                "namespace": "Alexa.Discovery",
                "name": "Discover.Response",
                "payloadVersion": "3",
                "messageId": messageId
            },
            "payload": {
                "endpoints": devices
            }
        }
    };

}

function PowerControllerResponse(header, endpoint) {
    mydeviceStatus[2] = "OFF"
    let power_state_value = "OFF";
    if (header.name === "TurnOn") {
        mydeviceStatus[2] = "ON"
        power_state_value = "ON";
        return {
            "context": {
                "properties": [
                    {
                        "namespace": "Alexa.PowerController",
                        "name": "powerState",
                        "value": power_state_value,
                        "timeOfSample": new Date().toISOString(),
                        "uncertaintyInMilliseconds": 200
                    },
                    {
                        "namespace": "Alexa.EndpointHealth",
                        "name": "connectivity",
                        "value": { "value": "OK" },
                        "timeOfSample": new Date().toISOString(),
                        "uncertaintyInMilliseconds": 200
                    }
                ]
            },
            "event": {
                "header": {
                    "namespace": "Alexa",
                    "name": "Response",
                    "payloadVersion": "3",
                    "messageId": header.messageId,
                    "correlationToken": header.correlationToken
                },
                "endpoint": endpoint,
                "payload": {}
            }
        };
    }
}

function ReportStateResponse(header, endpoint) {
    
    return {
        "context": {
            "properties": [
                {
                    "namespace": "Alexa.PowerController",
                    "name": "powerState",
                    "value": mydeviceStatus[2],
                    "timeOfSample": new Date().toISOString(),
                    "uncertaintyInMilliseconds": 200
                }
            ]
        },
        "event": {
            "header": {
                "namespace": "Alexa",
                "name": "StateReport",
                "payloadVersion": "3",
                "messageId": header.messageId,
                "correlationToken": header.correlationToken
            },
            "endpoint": endpoint,
            "payload": {}
        }
    };
}